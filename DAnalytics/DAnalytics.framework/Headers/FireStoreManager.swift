//
//  FireStoreManager.swift
//  DAnalytics
//
//  Created by Angshuk Nag on 15/05/18.
//  Copyright © 2018 Angshuk Nag. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

fileprivate struct FireStoreManagerConstants {
    static let minScreenWaitTime: Int = Int(5)
}

public enum DAnalyticsSessionState {
    case inProgress
    case stopped
}

public class DAnalticsManager: NSObject {
    var db: Firestore!
    var documentRef: DocumentReference?
    public static let sharedInstance = DAnalticsManager()
    public private(set) var sessionState: DAnalyticsSessionState = .stopped
    private(set) lazy var screenInfoStack = [ScreenInfoList]()
    private var currentSessionDict: [String: Any]?
    private var currentSessionDictBuffer: [String: Any]?
    private var screenStackInfoBuffer: [ScreenInfoList]?
    private var completion: DAnalyticsClosure.AddDocumentCallback?

    public static func shared() -> DAnalticsManager {
        return sharedInstance
    }

    public func configure(WithMinimumScreenWaitTime minScreenWaitTime: Int = Int(5)) {
        FirebaseApp.configure()
        db = Firestore.firestore()
    }

    @objc private func addData() {
        guard currentSessionDictBuffer != nil else {
            return
        }
        documentRef = db.collection("Analytics").addDocument(data: currentSessionDictBuffer!, completion: {[weak self] error in
            if let error = error {
                self?.completion?(.failure(error))
            } else {
                if let documentId = self?.documentRef?.documentID {
                    self?.sessionState = .inProgress
                    self?.updateStackAndSession()
                    self?.completion?(.success(documentId))
                } else {
                    self?.completion?(nil)
                }
            }
        })
    }

    func updateStackAndSession() {
        if let screenArray = screenStackInfoBuffer {
            screenInfoStack.append(contentsOf: screenArray)
        }
        if let screenInfo = currentSessionDictBuffer {
            currentSessionDict = screenInfo
        }
    }

    public func startSession(_ appVersion: String, deviceConfig: DeviceConfig, screenInfoList: ScreenInfoList, sessionStartTime: TimeInterval, type: String = "iOS", completion: DAnalyticsClosure.AddDocumentCallback?) throws {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(addData), object: nil)
        guard sessionState != .inProgress else {
            throw DAnalyticsError.sessionPresentFailure
        }

        var documentDict = ["appVersion": appVersion,
                            "timeStamp": sessionStartTime,
                            "type": type] as [String : Any]
        documentDict.merge(["deviceConfig": deviceConfig.getJSONDict()]) { (_, new) in new }
        documentDict.merge(["screenInfoList": [screenInfoList.getJSONDict()]]) { (_, new) in new }

        self.completion = completion
        currentSessionDictBuffer = documentDict
        screenInfoStack = [ScreenInfoList]()
        screenStackInfoBuffer = [screenInfoList]
        perform(#selector(addData), with: nil, afterDelay: TimeInterval(FireStoreManagerConstants.minScreenWaitTime))
    }

    public func endCurrentSession() {
        sessionState = .stopped
    }
    
    public func addScreen(_ screenInfoList: ScreenInfoList, completion: DAnalyticsClosure.AddDocumentCallback?) throws {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(addScreenNotify), object: nil)
        guard documentRef?.documentID != nil, sessionState != .stopped else {
            throw DAnalyticsError.addScreenError
        }

        screenStackInfoBuffer?.append(screenInfoList)
        if let screenArray = screenStackInfoBuffer?.map({ $0.getJSONDict() }) {
            currentSessionDictBuffer?["screenInfoList"] = screenArray
        }
        perform(#selector(addScreenNotify), with: nil, afterDelay: TimeInterval(FireStoreManagerConstants.minScreenWaitTime))
    }

    @objc private func addScreenNotify() {
        guard let documentId = documentRef?.documentID, screenStackInfoBuffer != nil, sessionState != .stopped else {
            return
        }

        let screenArray = screenStackInfoBuffer!.map { $0.getJSONDict() }
        db.collection("Analytics").document(documentId).updateData(["screenInfoList": screenArray]) {[weak self] error in
            if let error = error {
                self?.completion?(.failure(error))
            } else {
                self?.screenInfoStack = self!.screenStackInfoBuffer!
                self?.currentSessionDict = self!.currentSessionDictBuffer
                self?.completion?(.success("Successfully Added"))
            }
        }
    }

}
