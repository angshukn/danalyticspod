//
//  PublicEnum.swift
//  DAnalytics
//
//  Created by Angshuk Nag on 15/05/18.
//  Copyright © 2018 Angshuk Nag. All rights reserved.
//

import Foundation

public enum DAnalyticsError: Error {
    case sessionPresentFailure
    case addScreenError

    public var localizedDescription: String {
        switch self {
        case .sessionPresentFailure: return "Start Session failed: Session already present !!"
        case .addScreenError: return "Add Screen failed: No Session Info dictionary found !!"
        }
    }

}

/// A Generic enum for receving DAnalytics response data and error
///
/// - success: DAnalytics has successfull response
/// - failure: DAnalytics has failure response
public enum DAnalyticsResult<T, E> {
    case success(T)
    case failure(E)
    
    /// Boolean indicating whether API response is a success
    public var isSuccessful: Bool {
        switch self {
        case .success(_):
            return true
        default:
            return false
        }
    }
    
    /// API response data if any (if API response was success)
    public var result: T? {
        switch self {
        case .success(let result):
            return result
        default:
            return nil
        }
    }
    
    /// API error data if any (if API response was failure)
    public var error: E? {
        switch self {
        case .failure(let error):
            return error
        default:
            return nil
        }
    }
}
