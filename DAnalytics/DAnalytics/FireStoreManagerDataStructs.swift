//
//  FireStoreManagerDataStructs.swift
//  DAnalytics
//
//  Created by Angshuk Nag on 15/05/18.
//  Copyright © 2018 Angshuk Nag. All rights reserved.
//

import Foundation

public struct ScreenInfoList {
    var screenName: String!
    var startTime: TimeInterval!
    var endTime: TimeInterval!

    init(WithScreenName name: String, startDate: Date, endDate: Date) {
        self.screenName = name
        self.startTime = startDate.timeIntervalSince1970
        self.endTime = endDate.timeIntervalSince1970
    }
    
    init(WithData data: [String: Any]) {
        self.screenName = data["screenName"] as! String
        self.startTime = data["startTime"] as! TimeInterval
        self.endTime = data["endTime"] as! TimeInterval
    }

    public func getJSONDict() -> [String: Any] {
        return ["screenName": screenName,
                "startTime": startTime,
                "endTime": endTime]
    }

}

public struct DeviceConfig {
    var deviceName: String = "null"
    var location: String = "null"
    var osVersion: String = "null"
    var region: String = "null"
    var screenSize: String = "null"
    var vendor: String = "null"

    public func getJSONDict() -> [String: Any] {
        return ["deviceName": deviceName,
                "location": location,
                "osVersion": osVersion,
                "region": region,
                "screenSize": screenSize,
                "vendor": vendor]
    }

}
