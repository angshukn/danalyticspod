//
//  Closure.swift
//  DAnalytics
//
//  Created by Angshuk Nag on 15/05/18.
//  Copyright © 2018 Angshuk Nag. All rights reserved.
//

import Foundation

public struct DAnalyticsClosure {
    public typealias AddDocumentCallback = (DAnalyticsResult<String, Error>?) -> Void
}
